@TestOn('vm')

import 'package:test/test.dart';
import 'package:shelf/shelf.dart' as shelf;

import 'package:rest_api_server/api_server.dart';
import 'package:rest_api_server/src/route.dart';

void main() {
  test('print routes', () {
    final router = Router(routes, basePath: '/api');
    router.printRoutes();
    expect(() {
      router.printRoutes();
    },
        prints(
            'POST\t/api/action\nPOST\t/api/tests\nGET\t/api/tests/{id}/results\n'));
  });

  test('router handler', () {
    final router = Router(routes, basePath: '/api');
    final request =
        shelf.Request('POST', Uri.parse('https://api.server.com/api/action'));
    expect((router.handler(request) as shelf.Response).readAsString(),
        completion(equals('action')));
  });
}

final routes = <Route>[
  Route(
      'POST', 'action', (shelf.Request request) => shelf.Response.ok('action')),
  Route('POST', 'tests', (shelf.Request request) => shelf.Response.ok('tests')),
  Route('GET', 'tests/{id}/results',
      (shelf.Request request) => shelf.Response.ok('tests/{id}/results'))
];
