import 'dart:async';
import 'dart:convert';
import 'dart:io' show ContentType;

import 'package:data_model/data_model.dart';
import 'package:shelf/shelf.dart' as shelf;

/// Rote
class Route {
  /// Http-request method
  final String method;

  /// Request path
  final String path;

  /// Regular expression for quick path search
  final RegExp pathRegExp;

  /// route handler
  final shelf.Handler handler;

  /// Creates new routes
  Route(String method, this.path, this.handler)
      : assert(const ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']
            .contains(method.toUpperCase())),
        method = method.toUpperCase(),
        pathRegExp =
            RegExp('^' + path.replaceAll(RegExp(r'\{\w+\}'), '[^/]+') + r'$');
}

FutureOr<shelf.Response> makeResponseFrom(value) async {
  final result = value is Future ? await value : value;
  if (result is shelf.Response) return result;
  return shelf.Response.ok(json.encode(result, toEncodable: _toEncodable),
      headers: {'content-type': ContentType.json.toString()});
}

dynamic _toEncodable(value) {
  if (value is DateTime) return value.toUtc().toIso8601String();
  if (value is JsonEncodable) return value.json;
  if (value is Map) {
    return value.entries.fold<Map>({}, (result, entry) {
      final key = entry.key is JsonEncodable ? entry.key.json : entry.key;
      return result..[key] = entry.value;
    });
  }
  return '';
}
