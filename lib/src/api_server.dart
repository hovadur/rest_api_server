import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as io;

/// Rest Api server
///
///
class ApiServer {
  final InternetAddress _address;
  final int _port;
  final shelf.Handler _handler;

  HttpServer? _httpServer;

  /// Creates REST API-server
  ///
  /// [address] - address to listen
  /// [port] - port to listen
  /// [handler] - request hadler
  ApiServer({InternetAddress? address, int? port, shelf.Handler? handler})
      : _address = address ?? InternetAddress.loopbackIPv4,
        _port = port ?? (3000 + Random().nextInt(1000)),
        _handler = handler ??
            shelf.Pipeline().addHandler((request) async {
              final body = await request.readAsString();
              if (body == 'ping') {
                return shelf.Response.ok('pong');
              } else {
                return shelf.Response(HttpStatus.badRequest);
              }
            });

  /// Starts server
  Future start() async {
    _httpServer = await io.serve(_handler, _address, _port);
    print(
        'API-server is listening on ${_httpServer?.address.host}:${_httpServer?.port}');
  }

  /// Stops server
  Future stop() {
    final httpServer = _httpServer;
    if (httpServer == null) {
      throw (Exception('Server is not started'));
    } else {
      return httpServer.close();
    }
  }

  /// Server ip address
  InternetAddress get address {
    final httpServer = _httpServer;
    if (httpServer == null) {
      throw (Exception('Server is not started'));
    } else {
      return httpServer.address;
    }
  }

  /// Server port
  int get port {
    final httpServer = _httpServer;
    if (httpServer == null) {
      throw (Exception('Server is not started'));
    } else {
      return httpServer.port;
    }
  }
}
