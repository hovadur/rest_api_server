import 'dart:async';
import 'dart:convert';

import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart' hide Resource;
import 'package:code_builder/code_builder.dart';
import 'package:dart_style/dart_style.dart';
import 'package:glob/glob.dart';
import 'package:path/path.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:source_gen/source_gen.dart' hide LibraryBuilder;

import 'annotations.dart';

class ResourceInfoGenerator extends GeneratorForAnnotation<Resource> {
  @override
  Future<String> generateForAnnotatedElement(Element element,
      ConstantReader resourceMetaReader, BuildStep buildStep) async {
    if (_routableChecker.annotationsOf(element).length > 1) {
      throw RoutesBuildError(
          'Controller ${element.name} has concurrent annotations');
    }
    final resource = element as ClassElement;
    final resourcePath = resourceMetaReader.read('path').stringValue;
    final resourceMiddleware = resourceMetaReader.read('middleware');
    final emitter = DartEmitter();
    final prefix = '\$${buildStep.inputId.hashCode}\$';
    final routesInfo = resource.methods
        .where((method) => method.isPublic && method.metadata.isNotEmpty)
        .fold<List<String>>([], (routes, method) {
      if (_routableChecker.hasAnnotationOf(method)) {
        if (_routableChecker.annotationsOf(method).length > 1) {
          throw RoutesBuildError(
              "Multiply routable annotation of method ${method.name} in ${resource.name}");
        }
        final methodAnnotation = _routableChecker.firstAnnotationOf(method);
        final methodMetaReader = ConstantReader(methodAnnotation);
        final methodPath = methodMetaReader.read('path').stringValue;
        final methodMiddleware = methodMetaReader.read('middleware');
        final fullPath = _normalizePath(url.join(resourcePath, methodPath));
        final route = _generateRoute(
            methodAnnotation.type.toString().toUpperCase(),
            fullPath,
            _generateHandler(method, fullPath, prefix),
            resourceMiddleware,
            methodMiddleware);
        routes.add('${route.accept(emitter)}');
      }
      return routes;
    });
    final controller = _generateControllerField(resource, prefix);
    return json.encode({
      'routes': routesInfo,
      'uri': buildStep.inputId.uri.toString(),
      'prefix': prefix,
      'controller': '${controller.accept(emitter)}'
    });
  }

  final _routableChecker = TypeChecker.fromRuntime(Routable);

  Field _generateControllerField(ClassElement controller, String prefix) {
    return Field((b) {
      b
        ..modifier = FieldModifier.final$
        ..name = '_$prefix${controller.name}'
        ..assignment =
            CodeExpression(Code('$prefix.${controller.name}')).call([]).code;
    });
  }

  Expression _generateRoute(String httpMethod, String path, Method method,
      ConstantReader resourceMiddleware, ConstantReader methodMiddleware) {
    var handler = method.closure;
    if (!methodMiddleware.isNull) {
      handler = CodeExpression(Code(methodMiddleware.objectValue.toString()))
          .call([handler]);
    }
    if (!resourceMiddleware.isNull) {
      handler = CodeExpression(Code(resourceMiddleware.objectValue.toString()))
          .call([handler]);
    }

    return refer('Route')
        .call([literalString(httpMethod), literalString(path), handler]);
  }

  Method _generateHandler(MethodElement method, String path, String prefix) {
    final pathParameters = _extractPathParameters(path).fold<Map<String, Code>>(
        {},
        (params, param) => params
          ..addAll({
            param.value:
                Code('request.requestedUri.pathSegments[${param.key} + offset]')
          }));
    final controller = '_${prefix}${method.enclosingElement.name}';

    final handler = Method((builder) => builder
      ..requiredParameters.addAll([
        Parameter((builder) {
          builder
            ..type = refer('Request')
            ..name = 'request';
        })
      ])
      ..modifier = MethodModifier.async
      ..body = Block.of([
        if (pathParameters.isNotEmpty)
          refer('(List.from(request.requestedUri.pathSegments)..removeWhere((segment) => segment.isEmpty)).length')
              .operatorSubstract(literalNum(url.split(path).length))
              .assignFinal('offset')
              .statement,
        literalMap(pathParameters, refer('String'), refer('String'))
            .assignFinal('reqParameters')
            .statement,
        refer('reqParameters.addAll')
            .call([refer('request.requestedUri.queryParameters')]).statement,
        refer('$controller.${method.name}')
            .call(method.parameters
                .map((parameter) => _methodParameterParser(
                    parameter,
                    refer('reqParameters')
                        .index(literalString(parameter.name))))
                .where((element) => element != null)
                .whereType<Expression>())
            .assignFinal('result')
            .statement,
        refer('makeResponseFrom').call([refer('result')]).returned.statement
      ]));
    return handler;
  }

  String _normalizePath(String path) {
    var normalizedPath = url.normalize(path);
    if (url.isAbsolute(normalizedPath)) {
      normalizedPath = url.relative(normalizedPath, from: '/');
      normalizedPath = normalizedPath == '.' ? '' : normalizedPath;
    }
    return normalizedPath;
  }

  static final _pathParamRegexp = RegExp(r'^\{(.+)\}$');

  List<MapEntry<int, String>> _extractPathParameters(String path) => url
      .split(path)
      .asMap()
      .entries
      .map((entry) => MapEntry(
          entry.key, _pathParamRegexp.firstMatch(entry.value)?.group(1)))
      .where((entry) => entry.value != null)
      .whereType<MapEntry<int, String>>()
      .toList();

  Expression? _methodParameterParser(
      ParameterElement parameter, Expression value) {
    final requestTypeChecker = TypeChecker.fromRuntime(shelf.Request);
    if (parameter.type.isDartCoreString) {
      if (parameter.name == 'requestBody') {
        return refer('await request.readAsString()');
      }
    }
    if (parameter.type.isDartCoreNum)
      return value
          .equalTo(literalNull)
          .conditional(literalNull, refer('num.parse').call([value]));
    if (parameter.type.isDartCoreInt)
      return value
          .equalTo(literalNull)
          .conditional(literalNull, refer('int.parse').call([value]));
    if (parameter.type.isDartCoreDouble)
      return value
          .equalTo(literalNull)
          .conditional(literalNull, refer('double.parse').call([value]));
    if (parameter.type.isDartCoreString || parameter.type.isDynamic)
      return value;
    if (parameter.type.isDartCoreBool)
      return value.equalTo(literalNull).conditional(
          literalNull,
          value
              .property('toLowerCase')
              .call([]).equalTo(literalString('true')));
    if (parameter.type.isDartCoreMap) {
      switch (parameter.name) {
        case 'requestBody':
          return refer('json.decode(await request.readAsString())');
        case 'requestHeaders':
          return refer('request.headers');
        case 'context':
          return refer('request.context');
      }
    }
    if (requestTypeChecker.isExactlyType(parameter.type))
      return refer('request');
    return null;
  }
}

class RoutesBuilder extends Builder {
  static final _resourceFiles = Glob('lib/**.resource.info');

  static AssetId _routerOutput(BuildStep buildStep) {
    return AssetId(
      buildStep.inputId.package,
      url.join('lib', 'routes.g.dart'),
    );
  }

  @override
  Map<String, List<String>> get buildExtensions {
    return const {
      r'$lib$': ['routes.g.dart'],
    };
  }

  @override
  Future<void> build(BuildStep buildStep) async {
    final output = _routerOutput(buildStep);
    final libraryBuilder = LibraryBuilder();
    libraryBuilder.directives.addAll([
      Directive.import('dart:convert'),
      Directive.import('package:shelf/shelf.dart'),
      Directive.import('package:rest_api_server/src/route.dart')
    ]);
    final routes = <Code>[];
    await for (final input in buildStep.findAssets(_resourceFiles)) {
      final jsonString = (await buildStep.readAsString(input))
          .splitMapJoin(RegExp('(?://.*)?\n'), onMatch: (_) => '');
      final resourceInfo = json.decode(jsonString);
      libraryBuilder.directives.add(
          Directive.import(resourceInfo['uri'], as: resourceInfo['prefix']));
      libraryBuilder.body.add(Code('${resourceInfo['controller']}'));
      routes
          .addAll((resourceInfo['routes'] as List).map((route) => Code(route)));
    }
    libraryBuilder.body.add(
        literalList(routes, refer('Route')).assignFinal('routes').statement);
    final content = libraryBuilder.build().accept(DartEmitter());
    buildStep.writeAsString(output, DartFormatter().format('$content'));
    return;
  }
}

class RoutesBuildError extends Error {
  final message;

  RoutesBuildError([this.message]);

  String toString() => 'RouterBuildError: $message';
}
