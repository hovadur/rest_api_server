import 'dart:async';
import 'dart:io';

import 'package:data_model/data_model.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;

// ignore: uri_does_not_exist
import 'package:my_package/routes.g.dart' as generated;
import 'package:nanoid/nanoid.dart';
import 'package:rest_api_server/annotations.dart';
import 'package:rest_api_server/api_server.dart';
import 'package:rest_api_server/auth_middleware.dart';
import 'package:rest_api_server/cors_headers_middleware.dart';
import 'package:rest_api_server/http_exception_middleware.dart';
import 'package:rest_api_server/mongo_collection.dart';
import 'package:rest_api_server/src/service_registry.dart';
import 'package:shelf/shelf.dart' as shelf;

main() async {
// Define server address and port
  final address = InternetAddress.anyIPv4;
  final port = 7777;
// Open database connection
  final mongoDb = mongo.Db('mongodb://localhost/testdb');
  await mongoDb.open();
// Register services
  register<Jwt>(() => Jwt(
      securityKey: nanoid(),
      issuer: 'Some organization',
      maxAge: Duration(days: 1)));
  register<UsersCollection>(() => UsersCollection(mongoDb.collection('users')));
// Create router
  final router = Router(generated.routes, basePath: '/v1');

  ApiServer apiServer = ApiServer(
      address: address,
      port: port,
      handler: shelf.Pipeline()
          .addMiddleware(HttpExceptionMiddleware())
          .addMiddleware(CorsHeadersMiddleware({
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Expose-Headers': 'Authorization, Content-Type',
            'Access-Control-Allow-Headers':
                'Authorization, Origin, X-Requested-With, Content-Type, Accept, Content-Disposition',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE'
          }))
          .addMiddleware(AuthMiddleware(loginPaths: {
            'POST': ['/v1/login']
          }, exclude: const {
            'POST': ['/v1/login', '/v1/users'],
          }, jwt: locateService<Jwt>()))
          .addHandler(router.handler));
  await apiServer.start();

  router.printRoutes();
}

@Resource()
class Root {
  @Post(path: 'login')
  shelf.Response login() {
    return shelf.Response.ok('Ok', context: {
      'subject': 'any',
      'payload': {'role': 'any'}
    });
  }
}

@Resource(path: 'users')
class UsersResource {
  final UsersCollection usersCollection = locateService<UsersCollection>();

  @Post()
  Future<User> create(Map<String, dynamic> requestBody) =>
      usersCollection.insert(User.fromJson(requestBody));

  @Get(path: '{userId}')
  Future<User> getUser(String userId) =>
      usersCollection.findOne(UserId(userId));
}

class UserId extends ObjectId {
  UserId._(id) : super(id);

  factory UserId(id) {
    return UserId._(id);
  }
}

class User extends Model<UserId> {
  UserId? id;
  String? name;
  String? occupation;

  User({this.id, this.name, this.occupation});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: UserId(json['id']),
        name: json['name'],
        occupation: json['occupation']);
  }

  @override
  Map<String, dynamic> get json => {
        'id': id?.json,
        'name': name,
        'occupation': occupation
      }..removeWhere((key, value) => value == null);
}

class UsersCollection extends MongoCollection<User, UserId> {
  UsersCollection(mongo.DbCollection collection) : super(collection);

  @override
  User createModel(Map<String, dynamic> data) => User.fromJson(data);
}
